import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { MaterialModule } from '../material.module';
import { VsearchComponent } from '../components/vacation/vsearch/vsearch.component';
import { VrequestComponent } from '../components/vacation/vrequest/vrequest.component';
import { VbalanceComponent } from '../components/vacation/vbalance/vbalance.component';
import { VacationComponent } from '../components/vacation/vacation.component';
import { AppFeaturesComponent } from '../components/app-features/app-features.component';
import { HeaderComponent } from '../components/header/header.component';
import { VStatusHistoryComponent } from '../components/vacation/v-status-history/v-status-history.component';

@NgModule({
  entryComponents:[
    VbalanceComponent,VsearchComponent, VrequestComponent,VacationComponent,HeaderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,MaterialModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage,
        children:[
          {
            path: '',
            component: AppFeaturesComponent
           
          },
          {
            path: 'vacation',
            component: VacationComponent
           
          },
          {
            path: 'request',
            component: VrequestComponent
           
          },
          {
            path: 'search',
            component: VsearchComponent
           
          },
          {
            path: 'balance',
            component: VbalanceComponent
           
          },
          {
            path: 'status',
            component: VStatusHistoryComponent
           
          }

        ]
      },
      
     
     
     
      
    ]),
  ],
  //exports: [VacationPageModule],
  declarations: [HomePage,VacationComponent,VbalanceComponent,VsearchComponent,
     VrequestComponent, AppFeaturesComponent,HeaderComponent,VStatusHistoryComponent]
})
export class HomePageModule {}
