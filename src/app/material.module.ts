import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatTableModule,
  MatStepperModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatIconModule,
  MatPaginatorModule,
  MatSortModule,
  MatDatepickerModule,
  MatCardModule,
  MatNativeDateModule,
  MatToolbarModule,
  MatMenuModule,
  MatRippleModule, 
} from "@angular/material";
import { NgbdDatepickerIslamicUmalquraModule } from './shared/calender/datepicker-islamic-umalqura.module';
import {TableModule } from 'primeng/table';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgbdDatepickerIslamicUmalquraModule,
    TableModule
  ],
  exports:[
    MatTableModule,
    MatStepperModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule, 
    MatToolbarModule,
    MatMenuModule,
    MatRippleModule,
    NgbdDatepickerIslamicUmalquraModule, 
    TableModule
    
    
  ]
})
export class MaterialModule { }
