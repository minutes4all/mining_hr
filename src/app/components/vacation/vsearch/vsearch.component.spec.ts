import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VsearchComponent } from './vsearch.component';

describe('VsearchComponent', () => {
  let component: VsearchComponent;
  let fixture: ComponentFixture<VsearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VsearchComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
