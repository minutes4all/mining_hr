import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MandateComponent } from './mandate.component';

describe('MandateComponent', () => {
  let component: MandateComponent;
  let fixture: ComponentFixture<MandateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandateComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MandateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
