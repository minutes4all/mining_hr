import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { VbalanceComponent } from './components/vacation/vbalance/vbalance.component';
import { VsearchComponent } from './components/vacation/vsearch/vsearch.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
 
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    children: [
      {
        path: '',
        loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
      },
     
    ]
  },
  { path: '**', redirectTo: 'login' },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
