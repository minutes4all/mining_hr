import { Component, Injectable, ViewEncapsulation, OnInit, Input } from '@angular/core';

import { loadCldr, L10n, setCulture } from '@syncfusion/ej2-base';
// Here we have referred local json files for preview purpose
// tslint:disable-next-line: ban-types
declare let require: Function ;


@Component({
  selector: 'ngbd-datepicker-islamicumalqura',
  templateUrl: './datepicker-islamicumalqura.html',
  // providers: [
  //   {provide: NgbCalendar, useClass: NgbCalendarIslamicUmalqura},
  //   {provide: NgbDatepickerI18n, useClass: IslamicI18n}
  // ],
  encapsulation: ViewEncapsulation.None
})
// tslint:disable-next-line: component-class-suffix
export class NgbdDatepickerIslamicumalqura implements OnInit{
@Input() label;
  ngOnInit(): void {
  }

  constructor() 

{

    loadCldr(
      require('../../../../node_modules/cldr-data/supplemental/numberingSystems.json'),
      require('../../../../node_modules/cldr-data/main/ar/ca-islamic.json'),
      require('../../../../node_modules/cldr-data/main/ar/numbers.json'),
      require('../../../../node_modules/cldr-data/main/ar/timeZoneNames.json')
    );
    L10n.load({
      ar: {
      datepicker: {
        placeholder: "اختر تاريخا",
        today: "اليوم"
        }
      }
    });
  }
 
  

 
}
