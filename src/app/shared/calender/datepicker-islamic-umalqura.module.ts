import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbdDatepickerIslamicumalqura } from './datepicker-islamicumalqura';
import { DatePickerModule, DatePickerComponent } from '@syncfusion/ej2-angular-calendars';

@NgModule({
  imports: [ FormsModule,DatePickerModule],
  declarations: [NgbdDatepickerIslamicumalqura ],
  exports: [NgbdDatepickerIslamicumalqura],
  bootstrap: [NgbdDatepickerIslamicumalqura]
})
export class NgbdDatepickerIslamicUmalquraModule {}
