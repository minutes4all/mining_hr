import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-vsearch',
  templateUrl: './vsearch.component.html',
  styleUrls: ['./vsearch.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class VsearchComponent implements OnInit {
  cars: any=[];

  constructor() { }

  ngOnInit() {
    // tslint:disable-next-line: no-unused-expression
    this.cars=[{  
      vin: 'dsad231ff', year:'2012'	, brand: 'VW', color: 'Orange'},
      {  
        vin: '#@#', year:'2013'	, brand: 'BMW', color: 'red'},
        {  
          vin: 'dsad231ff', year:'207'	, brand: 'cc', color: 'yellow'}
  ]
  }
 // tslint:disable-next-line: no-unused-expression


  dataSource = ELEMENT_DATA;
  columnsToDisplay = ['عدد الأيام', 'من تاريخ', 'إلى تاريخ','تعديل','متابعة'];


  // columnsToDisplay = ['نوع الإجازة', 'عدد الأيام', 'من تاريخ', 'إلى تاريخ','تعديل','متابعة'];

  expandedElement: Vacation | null;

  
}

export class TableExpandableRowsExample {
 
}

export interface Vacation {
  name: string;
  position: string;
  weight: string;
  symbol: string;
  description: string;
}

const ELEMENT_DATA: Vacation[] = [
  {
    position: "2",
    name: 'Hydrogen',
    weight: "",
    symbol: 'H',
    description: `Hydrogen is a chemical element with symbol H and atomic number 1. With a standard
        atomic weight of 1.008, hydrogen is the lightest element on the periodic table.`
  },  ];

